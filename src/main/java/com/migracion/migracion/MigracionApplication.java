package com.migracion.migracion;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MigracionApplication {

	public static void main(String[] args) {
		SpringApplication.run(MigracionApplication.class, args);
	}

}
