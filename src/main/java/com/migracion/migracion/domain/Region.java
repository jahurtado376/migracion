package com.migracion.migracion.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;

@Entity
public class Region {

    @Id
    @Column(columnDefinition = "serial")
    private int idRegion;

    @NotEmpty(message = "El campo no puede estar vacio")
    @Size(min = 2, max = 50, message = "El tamaño del campo debe estar entre 2 y 50 caracteres")
    @Column(length = 50)
    private String regionName;


}
