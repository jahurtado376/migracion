package com.migracion.migracion.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;

@Entity
public class Municipality {

    @Id
    @Column(columnDefinition = "serial")
    private Long idMunicipality;

    @NotEmpty(message = "El campo no puede estar vacio")
    @Size(min = 2, max = 200, message = "El tamaño del campo debe estar entre 2 y 200 caracteres")
    @Column(length = 200)
    private String nameMunicipality;

    @NotEmpty(message = "El campo no puede estar vacio")
    @Size(min = 2, max = 2000, message = "El tamaño del campo debe estar entre 2 y 200 caracteres")
    @Column(length = 2000)
    private String codeMunicipality;

    @ManyToOne
    private Department department;

}
